\c dev
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
\c esupport
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
\c lucicloud
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
\c lucisky_qa
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
\c padb_harvest
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
\c template1
select userid, query, pid, datediff(minutes, starttime, sysdate) as minutes, left(text, 50) as text
from stv_inflight
where datediff(minutes, starttime, sysdate) > 30;
