#!/bin/bash
# Purpose: Detect long running queries in a given RDS instance
# Author: Trish Clark, 07/25/2016
# Prerequisites: 
# - Instance credentials must be stored at ~ubuntu/scripts/rds/.$INSTID.conf
# - Database platform is either MySQL or Redshift

# check for input arguments
if [ -z "$1" -o -z "$2" ]
then
    echo "Usage: $0 <PLATFORM> <INSTANCE NAME>"
    echo "Example: $0 mysql luciplusdb2"
    exit 1
fi

# declare variables
SCRIPTSDIR=/home/ubuntu/scripts
SQLDIR=$SCRIPTSDIR/sql
SHDIR=$SCRIPTSDIR/sh
SECUREDIR=$SCRIPTSDIR/rds
LOGDIR=$SCRIPTSDIR/log
OUTDIR=$SCRIPTSDIR/out
MYSQLQUERY=$SQLDIR/long_running_mysql.sql
REDSHIFTQUERY=$SQLDIR/long_running_redshift.sql
#REDSHIFTQUERY=$SQLDIR/long_running_redshift_fake.sql
ADDRESS=incidents@9fPqdQdthaA6vbvAqZtKaFNM.vistara.io

PLATFORM=$1
INSTID=$2
SECUREFILE=${SECUREDIR}/.${INSTID}.conf
LOGFILE=$LOGDIR/${INSTID}_check_long_running_sql.log
OUTFILE=$OUTDIR/${INSTID}_notify_long_running_sql.out

# ensure output files are cleared
rm -f $LOGFILE 
rm -f $OUTFILE
touch $LOGFILE
touch $OUTFILE

# check to see if credentials file exists for instance
if [ ! -f $SECUREFILE ]
then
    echo "Credentials file for $INSTID does not exist."
    exit 1
fi

# sql setup
source $SECUREFILE

if [ $PLATFORM = 'mysql' ]
then
    CONNSTRING="mysql -u$DBUSER -p$DBPASS -h$DBHOST"
    SQLCHECK=$MYSQLQUERY
elif [ $PLATFORM = 'redshift' ]
then
    CONNSTRING="psql -h $DBHOST -p $DBPORT -d dev -U $DBUSER"
    # get list of databases to loop through
    DBLIST=/tmp/dblist.lst
    $CONNSTRING -tc "select distinct datname from pg_database where datname != 'template0'" > $DBLIST
    # construct SQL to execute check on each database
    SQLCHECK=$SQLDIR/run_redshift_check.sql
    rm -f $SQLCHECK
    touch $SQLCHECK
    while read line
    do
	if [[ -n $line ]]
	then
	    printf "\c $line\n$(cat $REDSHIFTQUERY)\n" >> $SQLCHECK
	fi
    done < $DBLIST
else
    echo "Database platform '$PLATFORM' not currently in scope"
    exit 1
fi

# execute SQL to check for long running queries
$CONNSTRING < $SQLCHECK  > $LOGFILE

# define function for sending output
SENDOUT () {
msg=$1

echo -e "Long running query or queries have been detected. \n" >> $OUTFILE
printf "$msg" >> $OUTFILE
cat $OUTFILE | mailx -s "Long running query detected in $INSTID" $ADDRESS

}

# if there is output, send it
#LOGSTR=$(cat $LOGFILE | grep '[1-9]')
#if [[ -n $LOGSTR ]]
if [[ -s $LOGFILE && $PLATFORM = 'mysql' ]]
then
    SENDOUT "$(cat $LOGFILE)"
elif [[ $PLATFORM='redshift' ]]
then
    # check if any rows are greater than zero
    ROWDATA=$(grep 'row' $LOGFILE | grep '[1-9]')
    if [[ -n $ROWDATA ]]
    then
	SENDOUT "$(cat $LOGFILE)"
    fi
fi   

exit
